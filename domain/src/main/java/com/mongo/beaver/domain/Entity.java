package com.mongo.beaver.domain;

/**
 * @author yevhenii
 */
public interface Entity<T> {

    T getId();
}
