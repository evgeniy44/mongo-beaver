package com.edmunds.beaver.codec;

import com.edmunds.beaver.codec.domain.TestEntity;
import com.mongo.beaver.codec.EntityCodec;
import com.mongo.beaver.domain.Entity;
import org.bson.BsonBinaryReader;
import org.bson.ByteBufNIO;
import org.bson.Document;
import org.bson.codecs.DecoderContext;
import org.bson.io.ByteBufferBsonInput;
import org.testng.annotations.Test;

import java.nio.ByteBuffer;

import static org.testng.AssertJUnit.assertEquals;

/**
 * @author yevhenii
 */
public class EntityCodecTest {

    SerializationHelper helper = new SerializationHelper();

    @Test
    public void testDecodeString() {
        Document document = new Document("stringField", "string field value");
        byte[] serialized = helper.serialize(document);

        BsonBinaryReader binaryReader = new BsonBinaryReader(new ByteBufferBsonInput(new ByteBufNIO(ByteBuffer.wrap(serialized))));

        EntityCodec<TestEntity> entityCodec = new EntityCodec<TestEntity>(TestEntity.class);
        TestEntity entity = entityCodec.decode(binaryReader, DecoderContext.builder().build());
        assertEquals(entity.getStringField(), "string field value");
    }
}
