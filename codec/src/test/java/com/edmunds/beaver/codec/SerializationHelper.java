package com.edmunds.beaver.codec;

import org.bson.BsonBinaryWriter;
import org.bson.Document;
import org.bson.codecs.DocumentCodec;
import org.bson.codecs.EncoderContext;
import org.bson.io.BasicOutputBuffer;
import org.bson.io.BsonOutput;

/**
 * @author yevhenii
 */
public class SerializationHelper {

    public byte[] serialize(Document document) {
        DocumentCodec documentCodec = new DocumentCodec();
        BsonBinaryWriter writer = new BsonBinaryWriter(new BasicOutputBuffer());
        documentCodec.encode(writer, document, EncoderContext.builder().build());
        BsonOutput bsonOutput = writer.getBsonOutput();
        return ((BasicOutputBuffer)bsonOutput).toByteArray();
    }
}
