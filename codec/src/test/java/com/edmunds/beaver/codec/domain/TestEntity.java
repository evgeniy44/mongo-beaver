package com.edmunds.beaver.codec.domain;

import com.mongo.beaver.domain.Entity;

/**
 * @author yevhenii
 */
public class TestEntity implements Entity<String>{

    private String stringField;

    public String getStringField() {
        return stringField;
    }

    public void setStringField(String stringField) {
        this.stringField = stringField;
    }

    public String getId() {
        return "id";
    }
}
