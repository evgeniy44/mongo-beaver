package com.mongo.beaver.codec;

import com.mongo.beaver.domain.Entity;
import org.bson.BsonReader;
import org.bson.BsonType;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author yevhenii
 */
public class EntityCodec<T extends Entity> implements Codec<T> {

    private final Class<T> targetClass;
    private Map<String, Field> fields = new ConcurrentHashMap<String, Field>();

    public EntityCodec(Class<T> targetClass) {
        this.targetClass = targetClass;
        for (Field field : targetClass.getDeclaredFields()) {
            field.setAccessible(true);
        }
    }

    public T decode(BsonReader bsonReader, DecoderContext decoderContext) {
        T entity = createInstance();
        bsonReader.readStartDocument();
        while (bsonReader.getCurrentBsonType() != BsonType.END_OF_DOCUMENT) {
            String name = bsonReader.readName();
            Field field = getFieldByName(name);
            readNextValue(field, bsonReader, entity);
            bsonReader.readBsonType();
        }
        return entity;
    }

    private void readNextValue(Field field, BsonReader bsonReader, T entity) {
        Object value;
        if (bsonReader.getCurrentBsonType() == BsonType.STRING){
            value = bsonReader.readString();
        } else {
            throw new UnsupportedOperationException();
        }
        try {
            field.set(entity, value);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private Field getFieldByName(String name) {
        if (fields.containsKey(name)) {
            return fields.get(name);
        } else {
            Field field = null;
            try {
                field = targetClass.getDeclaredField(name);
            } catch (NoSuchFieldException e) {
                throw new RuntimeException(e);
            }
            field.setAccessible(true);
            fields.put(name, field);
            return field;
        }
    }

    private T createInstance() {
        try {
            return targetClass.newInstance();
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    public void encode(BsonWriter bsonWriter, Entity entity, EncoderContext encoderContext) {
        throw new UnsupportedOperationException();
    }

    public Class<T> getEncoderClass() {
        return targetClass;
    }
}
